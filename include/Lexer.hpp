#ifndef LEXER_H
#define LEXER_H

#if !defined(yyFlexLexer)
#include <FlexLexer.h>
#endif

#include "Parser.hpp" // for cn::Parser::symbol_type

#undef YY_DECL
#define YY_DECL cn::Parser::symbol_type cn::Lexer::cnlex()

namespace cn {

class Driver;

class Lexer : public yyFlexLexer
{
private:
    Driver &driver;
public:
    Lexer(Driver &driver) : driver(driver) {}
    virtual ~Lexer() {}
    virtual cn::Parser::symbol_type cnlex();
};

}

#endif
