#ifndef OPTIONHANDLER_H
#define OPTIONHANDLER_H

#include <iostream>
#include <string>
#include <getopt.h> // Note: POSIX & GNU, not portable.

#include "Log.hpp"

namespace cn
{

class OptionHandler
{
private:
    // TODO Use a better and more ubiquitous solution for version string.
    std::string const VERSION_STRING = "cnatural version 0.0.0";

    std::string const HELP_STRING =
        "cn [options] files...\n"
        "\n"
        "Options:\n"
        "-h --help\n"
        "   Show this help message.\n"
        "-V --version\n"
        "   Show software version.\n"
        "-v --verbose\n"
        "   Provide detailed logging information.\n"
        "-o --output-dir <dir>\n"
        "   Output target source files in <dir> instead of present working directory.\n"
        "--preserve-dir\n"
        "   Output target source files to directory of input source file.\n"
        "--trace-lexer\n"
        "--trace-parser\n"
        "   Output a complete lexer/parser trace.";

    // Option Variables
    int _preserveDir = false;
    std::string _outputDir;
    int _traceLexer = false;
    int _traceParser = false;

    struct option const long_options[8] = {
        {"help",          no_argument,        0, 'h' },
        {"version",       no_argument,        0, 'V' },
        {"verbose",       no_argument,        0, 'v' },
        {"output-dir",    required_argument,  0, 'o' },
        // Opts with no short version:
        {"preserve-dir",  no_argument,        &_preserveDir, true },
        {"trace-lexer",   no_argument,        &_traceLexer, true },
        {"trace-parser",  no_argument,        &_traceParser, true },
        {0,             0,                    0,  0  }
    };

public:
    int optionIndex = 0;

    OptionHandler(int argc, char *argv[]);

    int preserveDir();
    std::string outputDir();
    int traceLexer();
    int traceParser();
};

}

#endif
