#ifndef DRIVER_H
#define DRIVER_H

#include <string>

#include "Lexer.hpp"
#include "Parser.hpp"
#include "FileContext.hpp"
#include "OptionHandler.hpp"

namespace cn {

class Driver
{
public:
    // TODO We should encapsulate these member variables properly instead of exposing them publicly.
    // Is friendliness actually a good idea here?
    OptionHandler _opts;
    Lexer _lexer;
    Parser _parser;
    FileContext *_fileContext;
//public:
    Driver(OptionHandler opts);

    int parse(std::string &filename);
};

}

#endif
