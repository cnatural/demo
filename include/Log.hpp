#ifndef LOG_H
#define LOG_H

#include <iostream>

namespace cn
{

enum class Log
{
    Critical,
    Error,
    Warning,
    Verbose
};

extern Log GLOBAL_LEVEL;

// Thanks to https://stackoverflow.com/a/59673391
class NullStream : public std::ostream
{
public:
    NullStream() :
        std::ostream(nullptr)
    {}

    NullStream(const NullStream&) :
        std::ostream(nullptr)
    {}
};

// For reasons unknowable, overloading this global operator function is quite
// different from overloading it within the class definition itself.
template <typename T>
const NullStream &operator<<(NullStream &&ns, T const &value)
{
      return ns;
}

extern NullStream nullstream;

std::ostream &log(Log const &level);

}

#endif
