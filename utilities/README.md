# Utilities

## Compile Script
- cmake . -DCMAKE_INPUT_FILE={name of file}
    - (where {name of file} is the file name)
    - Do not add file extension
    - Only has to be run once
- make

## importlib
- #importlib \<file to import>
    - e.g: #importlib \<SDL2>

## Debugger
- Compile cn -> cpp & compile cpp
- Run debugger in same directory as cn files
