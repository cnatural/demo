CMAKE_MINIMUM_REQUIRED(VERSION 3.16)

PROJECT(compileCN)

IF(${CMAKE_INPUT_FILE} MATCHES ".cn")
    string(REGEX REPLACE ".cn" "" CMAKE_INPUT_FILE ${CMAKE_INPUT_FILE})
ENDIF() 

IF(EXISTS ${CMAKE_INPUT_FILE}.cn)
    file(READ ${CMAKE_INPUT_FILE}.cn READ_FILE)
ELSE()
    MESSAGE("No valid file given. Please rerun with flag -DCMAKE_INPUT_FILE=<file name>")
    return()
ENDIF()
STRING(REGEX REPLACE "\n" ";" READ_FILE "${READ_FILE}")
SET(LIST "")
SET(FILES "")

FOREACH(LETTER ${READ_FILE})
    IF(${LETTER} MATCHES "#importlib <")
        string(REPLACE "#importlib <" "" LETTER ${LETTER})
        string(REGEX REPLACE ">" ";" LETTER ${LETTER})
        list(APPEND LIST ${LETTER}) 
    ELSEIF(${LETTER} MATCHES "#include \"" AND ${LETTER} MATCHES ".hpp")
        string(REGEX REPLACE "#include \"" "" LETTER ${LETTER})
        string(REGEX REPLACE ".hpp\"" "" LETTER ${LETTER})
        list(APPEND FILES ${LETTER})
    ENDIF()

ENDFOREACH()


SET(LINKED_FILES "")

FOREACH(FILE ${FILES})
    exec_program(cn ARGS ${FILE}.cn)

    list(APPEND LINKED_FILES ${FILE}.cpp)
ENDFOREACH()

exec_program(cn ARGS ${CMAKE_INPUT_FILE}.cn)

add_executable(${CMAKE_INPUT_FILE} ${CMAKE_INPUT_FILE}.cpp ${LINKED_FILES})
target_link_libraries(${CMAKE_INPUT_FILE} ${LIST})
