#include "Log.hpp"

namespace cn
{

// Initialise extern-ed global values.

// Default logging level.
Log GLOBAL_LEVEL = Log::Warning;

// Global instance of dummy stream.
NullStream nullstream = NullStream();

std::ostream &log(Log const &level)
{
    if (level <= GLOBAL_LEVEL)
    {
        return std::clog;
    }
    else
    {
        return nullstream;
    }
}

}
