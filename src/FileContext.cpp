#include "FileContext.hpp"

#include <regex>

#if (__GNUC__ < 8)
#include <experimental/filesystem>
namespace filesystem = std::experimental::filesystem;
#else
#include <filesystem> // C++17. Constrained to here, might replace with external lib for wider version support.
namespace filesystem = std::filesystem;
#endif

using namespace cn;

FileContext::FileContext(std::string &filepath, OptionHandler &opts) :
    _opts(opts)
{
    scopeInfoStack.push(std::make_unique<ScopeInfo>());
    prevExprScopeInfo = std::make_unique<ScopeInfo>();
    _location.initialize(&filepath);
    initializeOutputStreams(filepath);
}

FileContext::~FileContext()
{
    outHpp.close();
    outCpp.close();
}

void FileContext::initializeOutputStreams(std::string const &filepath)
{
    filesystem::path path(filepath);
    filesystem::path outpath;

    if (!_opts.outputDir().empty())
    {
        outpath = _opts.outputDir() / path.filename();
    }
    else if (_opts.preserveDir())
    {
        outpath = path;
    }
    else
    {
        outpath = path.filename();
    }

    outHpp = std::ofstream(outpath.replace_extension("hpp").string());
    outCpp = std::ofstream(outpath.replace_extension("cpp").string());

    std::string outstem = outpath.stem().string();
    // Link cpp file to hpp.
    selfIncludeStr = "#include \"" + outstem + ".hpp\"";
    // Add header guard to hpp.
    outHpp << "#ifndef " << outstem << "_H" << std::endl
           << "#define " << outstem << "_H" << std::endl;
}

void FileContext::selfInclude(char const endchar)
{
    if (!selfIncludeFlag)
    {
        outCpp << selfIncludeStr;
        if (endchar)
            outCpp << endchar;
        selfIncludeFlag = true;
    }
}

void FileContext::writeScopeChanges(std::ofstream &fout, size_t indentCurr, size_t indentPrev)
{
    long long scopeDiff = indentCurr - indentPrev;

    if (scopeDiff > 0)
    {
        // If scope has increased, we need to add the previous line's scope info plus additional scopes.
        // If the previous line has no scope info, set to default.
        if (!prevExprScopeInfo)
        {
            prevExprScopeInfo = std::make_unique<ScopeInfo>();
        }

        scopeInfoStack.push(std::move(prevExprScopeInfo));
        fout << '{';
        for (long long i = 1; i < scopeDiff; ++i)
        {
            scopeInfoStack.push(std::make_unique<ScopeInfo>());
            fout << '{';
        }
    }

    if (scopeDiff <= 0)
    {
        // If scope hasn't increased, the previous expression needs to be terminated with a semicolon.
        fout << ';';

        // If scope has decreased, we need to close that many scopes.
        for (long long i = 0; i < -scopeDiff; ++i)
        {
            fout << scopeInfoStack.top()->close();
            scopeInfoStack.pop();
        }
    }
}

void FileContext::handleStructKeyword()
{
    // TODO It would be good to auto-typedef structs.
    prevExprScopeInfo = std::make_unique<ScopeInfoSemiclose>();
    scopeFloorIncrement = true;
}

void FileContext::handleClassKeyword(std::string const &expr)
{
    std::regex r("^class (\\w+).*");
    std::smatch matches;

    if (std::regex_match(expr, matches, r))
    {
        std::string new_prefix = scopeInfoStack.top()->prefix() + matches[1].str() + "::";
        prevExprScopeInfo = std::make_unique<ScopeInfoSemiclose>(new_prefix);
        scopeFloorIncrement = true;
    }
    else
    {
        log(Log::Warning) << "No class identifier found in \"" << expr << "\"." << std::endl;
    }
}

void FileContext::writeExpr(size_t indentCurr, std::string const &currExpr)
{
    // If above indent floor, write to cpp.
    // In other words, if EXPR contains function definition code, not other definitions and declarations, write to cpp.
    if (indentCurr > scopeFloor)
    {
        // If the last expression was at the scope floor, it needs to be in both the hpp and cpp files.
        // We didn't write the last expression to cpp because it was at the floor, but we kept it in prevExpr.
        if (indentPrev <= scopeFloor)
        {
            // Naively check if expr is a function.
            // In the future, this will be handled by the scanner.
            // TODO Until we support this in the scanner, a class on the first line of a function will not scope properly.
            // Luckily, we don't technically support that at all yet ;)
            std::string temp = prevExpr.substr(0, prevExpr.find_first_of('('));
            if (temp.length() != prevExpr.length())
            {
                // Find start of function name identifier.
                size_t funcsig_it = temp.find_last_of(" *&") + 1;

                std::string prefuncsig = prevExpr.substr(0, funcsig_it);

                // Remove virtual keyword.
                std::regex regexVirtual("^virtual | virtual ");
                prefuncsig = std::regex_replace(prefuncsig, regexVirtual, "");

                std::string postfuncsig = prevExpr.substr(funcsig_it);
                // Remove default arguments.
                // TODO This regex does not match strings and chars containing a space, ,, or ).
                // It _improves_ support for default arguments, but it does not _implement_ it.
                // Function handling is all a bit quick and dirty at the moment.
                std::regex regexDefault("([^ ,)]+) ?= *[^ ,)]+");
                postfuncsig = std::regex_replace(postfuncsig, regexDefault, "$1");

                // Concatenate function type, function prefix (usually name of
                // containing class), and function signature.
                // Seek back 1 char to try and keep
                outCpp.seekp(-1, std::ios::end);
                outCpp << prefuncsig << scopeInfoStack.top()->prefix() << postfuncsig;
                outCpp.seekp(0, std::ios::end);
                outCpp << '\n';
            }
            else
            {
                outCpp << prevExpr << std::endl;
            }
        }

        writeScopeChanges(outCpp, indentCurr, indentPrev);
        outCpp << currExpr;
    }
    // If move below or stay on floor, close remaining brackets in implementation and lower floor or add semicolons to hpp.
    else
    {
        // We don't want changes to the scope floor to affect the current iteration.
        // So we assign to a temporary value and set the scopeFloor var at the end.
        size_t newScopeFloor = scopeFloor;

        if (scopeFloorIncrement)
        {
            ++newScopeFloor;
            scopeFloorIncrement = false;
        }
        else
        {
            newScopeFloor = indentCurr;
        }

        if (indentPrev > scopeFloor)
        {
            // Only change scope for cpp down to the scopeFloor, anything below that is due to something written in the header.
            writeScopeChanges(outCpp, scopeFloor, indentPrev);
            writeScopeChanges(outHpp, indentCurr, scopeFloor);
        }
        else
        {
            writeScopeChanges(outHpp, indentCurr, indentPrev);
        }

        scopeFloor = newScopeFloor;

        outHpp << currExpr;
    }
}
