%{
    #include "Driver.hpp"
    #include "FileContext.hpp"

    #include "Parser.hpp"
    #include "location.hh"

    static cn::Parser::symbol_type yylex(cn::Driver &driver) {
        return driver._lexer.cnlex();
    }

    using namespace cn;
%}

%skeleton "lalr1.cc" // Use C++
%require "3.3" // We require at least Bison 3, C++ skeleton changes mean safest for us to use 3.3
%defines

// Parser class is at cn::Parser
%define api.namespace { cn }
%define api.parser.class { Parser }

// Allow direct use of C++ types.
%define api.value.type variant
// Use make_{TOKEN} to create instances of typed tokens in the lexer.
// We get type safety and rich location tracking for free!
%define api.token.constructor
%define parse.assert

%code requires
{
    #include <iostream>
    #include <string>

    namespace cn {
        class Lexer;
        class Driver;
        class FileContext;
    }

}

%param { cn::Driver &driver }
// We have no way to change the Parser class implementation directly, so we
// put most of the functionality in FileContext and give it as a parameter.
%parse-param { cn::FileContext *&ctx }

%locations

%define parse.trace
%define parse.error verbose

%define api.token.prefix {TOK_}

%token END 0 "end of file";
%token NL "newline";
%token <size_t> INDENT "indentation";

// Expressions
%token <std::string> EXPR "expression";
%token <std::string> CLASS "class defintion";
// Macros
%token <std::string> COMMENT "comment";
%token <std::string> MACRO "macro";
// Macro Extensions
%token IMPORTLIB_MACRO "importlib macro";

%type <std::string> macroline;
%type <std::string> exprline;

%start codefile

%%


codefile:
    %empty
        {
            // Anything to init at the start of the file should go here.
        }
    | codefile line
        {
            // Anything to be done at the end of every line should go here.
            ctx->outHpp << '\n';
            ctx->outCpp << '\n';
        }
    ;

line:
    emptyline
        {
            ctx->selfInclude();
        }
    | macroline NL
        {
            ctx->selfInclude();
            ctx->prevExpr = $1;
        }
    | exprline NL
        {
            ctx->prevExpr = $1;
        }
    | COMMENT NL
        {
            // We don't currently add comments to the output file.
            ctx->selfInclude();
            //ctx->writeExpr(ctx->indentPrev, $1);
        }
    ;

emptyline:
    NL
        { }
    | INDENT NL
        { }
    ;

macroline:
    MACRO
        {
            ctx->outHpp << $1;
            $$ = $1;
        }
    | IMPORTLIB_MACRO
        {
            // #IMPORTLIB is a macro for the external cn compiler scripts.
            // As they handle it, we only need to exclude it from the compiled output.
        }
    ;

exprline:
    EXPR
        {
            ctx->selfInclude('\n');
            ctx->writeExpr(0, $1);
            ctx->indentPrev = 0;
            $$ = $1;
        }
    | INDENT EXPR
        {
            ctx->selfInclude('\n');
            ctx->writeExpr($1, $2);
            ctx->indentPrev = $1;
            $$ = $2;
        }

%%

void cn::Parser::error(location_type const &loc, std::string const &message)
{
        std::cout << loc << ": " << message << std::endl;
}
